
=begin

もんむす・くえすと！ＲＰＧ
　トリス修正E  ver2  2014/12/05



機能一覧　説明は下　統合済みのものは×
×キャラ図鑑にもCG閲覧機能


機能　説明
×キャラ図鑑にもCG閲覧機能
　設定箇所：SettingData/Library(Actor).rb 内の
　　　　　　　ACTOR_DEFAULT_MEMORY_BG_IMAGE
　　　　　　　ACTOR_MEMORY_BG_IMAGE
　　　　　　　ACTOR_CG_VIEW_IMAGE
　　　　　　の３つ　設定形式は、エネミーのＣＧ閲覧と同様
            
　キャラ図鑑で決定ボタンを押すと「ＣＧ閲覧　キャンセル」の選択肢が出る
　ＣＧ閲覧は『戦闘中でなく、そのアクターの好感度変数が100以上』の時のみ選択可能

　またエネミー図鑑の選択肢を「敗北回想　ＣＧ閲覧　キャンセル」の３つに


=end